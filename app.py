from flask import Flask, render_template, request
from owlready2 import *
import plotly.express as px
import pandas as pd
import json
import plotly
import string

app = Flask(__name__)

# Bouchon 
r = {'Extraversion' : -0.5, 'Neuroticism' : 0.5, 'Conscientiousness' : 0.2, 'Openness' : 0.3, 'Agreeableness' : 0.1}

def get_class_and_corr_of_word(onto, word) :
    """Renvoie la classe et le type de corrélation du mot.

    Args:
        onto (Object): Ontologie
        word (string): Le mot

    Returns:
        (string, int): La classe du mot et le type de corrélation
    """

    classe = None
    type = None
    # Récupère toutes les instances comme une liste
    all_instances = [i.name for i in onto.individuals()]
    if word in all_instances :
        classes = list(onto[word].__class__.is_a)
        if len(classes) > 1 :
            classe = classes[0].is_a[0].name 
        else :
            classe = onto[word].__class__.is_a[0].name
        type = getattr(onto[word],"CorelationType")[0]
        return (classe,type)
    else : return None

def get_score_from_text(onto, text) :
    """ Renvoie la grille score big five du texte.

    Args:
        onto (Object): Ontologie
        text (string): Le texte

    Returns:
        dict : Renvoie un score pour chaque traits entre 0 et 1.
    """
    score = None
    
    # Création automatique de la grille de score
    bigFiveTraits = list(onto["BigFiveTraits"].subclasses())
    score = {x.name:0 for x in bigFiveTraits}
    # Ittération sur le texte pour ajouter les occurences
    text = ''.join([x for x in text if x in string.ascii_letters + '\'- '])
    text_as_array = text.split(' ')
    for word in text_as_array :
        ans = get_class_and_corr_of_word(onto, word)
        if ans is not None :
            score[ans[0]] += ans[1]
    
    # Normalisation de la grille (>=0 et 1<=)
    max_value = max(score.values())
    min_value = min(score.values())
    if max_value != 0 :
        for key,value in score.items() :
            score[key] = (value - min_value) / (max_value - min_value)
    return score

@app.route("/")
def home():
    return render_template('home.html')


@app.route('/', methods=['POST'])
def home_with_processing():
    form = request.form
    text = form['text']
    
    onto = get_ontology("./big_five_v1.owl")
    onto.load()
    r = get_score_from_text(onto, text)

    df = pd.DataFrame(dict(
    r=r.values(),
    theta=r.keys()))
    fig = px.line_polar(df, r='r', theta='theta', line_close=True,width=800, height=400)
    fig.update_traces(fill='toself')
    graphJSON = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)
    return render_template('home.html', graphJSON=graphJSON)


