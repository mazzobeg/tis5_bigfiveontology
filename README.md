# TIS5_bigFiveOntology

1. Installer les dépendances
`python -m pip install -r requirements.txt`
2. Lancer le serveur
`python -m flask run`
3. Copier l'addresse IP local fournie en ligne de commande dans un navigateur web

## Example

- Coller le texte suivant dans le champ d'insertion 
> Service makes our talents bear fruit and gives meaning to our lives. Those who do not live to serve, serve for little in this life.
I thank all those who are committed to alleviating their suffering. Let us remember migrants, their suffering and #PrayTogether.
I feel sorrow thinking about those who died crossing the English Channel, those on the bordersof Belarus, many of whom are children, those who drown in the Mediterranean, those who are repatriated to North Africa and forced into servitude.
Even during these days so many migrants are exposed to very serious dangers, and so many lose their lives at our borders!
In the #GospelOfTheDay, Jesus exhorts us to be vigilant: not to allow our hearts to become lazy. We cannot be "sleepy Christians" without spiritual fervor, withour intensity in prayer, without passion for the Gospel.
Our life becomes beautiful when we wait for a dear one or someone important. May this #Advent help us transform our hope into the certainty that He whom we await loves us and will never abandon us.
Let us be fearless amid the messy situations all around us, because that is where the Lord is,in our midst; God continues to perform his miracle of bringing forth good fruit (Jn 15:5). Christian joy is born precisely of this certainty.
The prayer of adoration is the prayer that makes us recognize God as the beginning and the end of all of History. And this prayer is the living flame of the Spirit that gives strength to witness and to mission.
Let us not to forget to thank God. If we are bearers of gratitude, the world itself will become better, even if only a little bit, but that is enough to transmit a bit of hope. Everything is united and connected, and each one can do their part wherever they are. #Thanksgiving
The various forms of ill-treatment that many women suffer are acts of cowardice and a degradation of all humanity. We must not look the other away. Women who are victims of violence must be protected by society.
